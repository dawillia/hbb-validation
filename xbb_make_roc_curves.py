#!/usr/bin/env python3

"""
Make histograms of the jet pt spectra
"""

from argparse import ArgumentParser
from h5py import File
from glob import glob
import numpy as np
import json, os

from xbb.common import get_denom_dict, get_dsid
from xbb.common import is_dijet, is_ditop, is_dihiggs
from xbb.common import SELECTORS
from xbb_draw_roc_curves import draw_roc_curves
from xbb.cross_section import get_xsecs
from xbb.selectors import (
    mass_window_higgs, mass_window_top, window_pt_range,
    window_pt_range_truth_match)

# default settings
PT_RANGE = (250e3, np.inf)

def get_args():
    parser = ArgumentParser(description=__doc__)
    d = 'default: %(default)s'
    parser.add_argument('datasets', nargs='+')
    parser.add_argument('-d', '--denominator', required=True)
    parser.add_argument('-x', '--cross-sections', required=True)
    parser.add_argument('-i', '--input-hist-dir', default='pt-hists')
    parser.add_argument('-o', '--output-dir', default='plots')
    parser.add_argument('-p', '--pt-range',
                        nargs=2, type=float, default=PT_RANGE, help=d)
    parser.add_argument('-s', '--save-file')
    parser.add_argument('-t', '--target', default='higgs',
                        help='target pt spectrum, default: %(default)s')
    parser.add_argument('-v', '--verbose', action='store_true')
    return parser.parse_args()

####################
# variable getters #
####################

# various constants
SJ = 'subjet_VR_{}'
SJ_FR = 'subjet_FR_{}'
SJ_GHOST = 'subjet_VRGhostTag_{}'

def get_mv2(h5file, discriminant='MV2c10_discriminant', fpath=''):
    disc1 = h5file[SJ.format(1)][discriminant]
    disc2 = h5file[SJ.format(2)][discriminant]
    discrim_comb = np.stack([disc1, disc2], axis=1).min(axis=1)
    invalid = np.isnan(discrim_comb)
    discrim_comb[invalid] = -1.0
    return discrim_comb

def get_dnn(h5file, fpath=''):
    return np.asarray(h5file['fat_jet']['HbbScore'])

def make_xbb_getter(ftop=0.5):
    def get_xbb(h5file,ftop=ftop, fpath=''):
        fj = h5file['fat_jet']['XbbScoreHiggs', 'XbbScoreQCD', 'XbbScoreTop']
        num = fj['XbbScoreHiggs']
        denom_qcd = fj['XbbScoreQCD'] * (1 - ftop)
        denom_top = fj['XbbScoreTop'] * ftop
        denom = denom_qcd + denom_top
        valid = (denom != 0.0) & (num != 0.0) & (np.isfinite(num))
        ret_vals = np.empty_like(num)
        ret_vals[valid] = np.log(num[valid] / denom[valid])
        ret_vals[~valid] = -10.0
        return ret_vals
    return get_xbb

def make_prediction_getter(ftop=0.5):
    def get_prediction(h5file,ftop=ftop, fpath=''):
        # Look for a prediction file for the current sample, need to edit to point to your prediction files
        fname = '../Xbb_training/predictOptimization/Predictions_samples_newVRalg_0919_mc16a/Prediction_'+fpath.split('/')[-1]
        try:
            fat_jet = File(fname, 'r')
        except OSError:
            print(f'{fname} not found')
            return []
        fj = fat_jet['data'][()]
        num = fj[:,1]
        denom_qcd = fj[:,0] * (1 - ftop)
        denom_top = fj[:,2] * ftop
        denom = denom_qcd + denom_top
        valid = (denom != 0.0) & (num != 0.0) & (np.isfinite(num))
        ret_vals = np.empty_like(num)
        ret_vals[valid] = np.log(num[valid] / denom[valid])
        ret_vals[~valid] = -10.0
        return ret_vals
    return get_prediction


def get_top_vs_qcd(h5file, fpath=''):
    fj = h5file['fat_jet']['XbbScoreQCD', 'XbbScoreTop']
    num = fj['XbbScoreTop']
    denom = fj['XbbScoreQCD']
    valid = (denom != 0.0) & (num != 0.0) & (np.isfinite(num))
    ret_vals = np.empty_like(num)
    ret_vals[valid] = np.log(num[valid] / denom[valid])
    ret_vals[~valid] = -10.0
    return ret_vals

def get_tau32(h5file, fpath=''):
    vals = 1.0 - h5file['fat_jet']['Tau32_wta']
    vals[~np.isfinite(vals)] = 0.0
    return vals

def get_jsstop(h5file, fpath=''):
    vals = h5file['fat_jet']['JSSTopScore']
    vals[~np.isfinite(vals)] = 0.0
    return vals

def make_dl1_getter(subjet=SJ):
    def get_dl1(h5file, subjet=subjet, fpath=''):
        def dl1_sj(subjet, f=0.08):
            sj = h5file[subjet]['DL1r_pb','DL1r_pu','DL1r_pc']
            return sj['DL1r_pb'] / ( (1-f) * sj['DL1r_pu'] + f * sj['DL1r_pc'])

        disc1 = dl1_sj(subjet.format(1))
        disc2 = dl1_sj(subjet.format(2))
        discrim_comb = np.stack([disc1, disc2], axis=1).min(axis=1)
        invalid = np.isnan(discrim_comb) | np.isinf(discrim_comb)
        discrim_comb[invalid] = 1e-15
        return np.log(np.clip(discrim_comb, 1e-30, 1e30))
    return get_dl1

def make_vr_dl1r_getter(subjet=SJ):
    def get_dl1(h5file, subjet=subjet, fpath=''):
        def dl1_sj(subjet, f=0.08):
            sj = h5file[subjet]['vr_dl1r_pb','vr_dl1r_pu','vr_dl1r_pc']
            return sj['vr_dl1r_pb'] / ( (1-f) * sj['vr_dl1r_pu'] + f * sj['vr_dl1r_pc'])

        disc1 = dl1_sj(subjet.format(1))
        disc2 = dl1_sj(subjet.format(2))
        discrim_comb = np.stack([disc1, disc2], axis=1).min(axis=1)
        invalid = np.isnan(discrim_comb) | np.isinf(discrim_comb)
        discrim_comb[invalid] = 1e-15
        return np.log(np.clip(discrim_comb, 1e-30, 1e30))
    return get_dl1

#################################
# functions that do real things #
#################################
def get_hist(ds, edges, discriminant, selection=mass_window_higgs):
    hist = 0
    for fpath in glob(f'{ds}/*.h5'):
        with File(fpath,'r') as h5file:
            fat_jet = np.asarray(h5file['fat_jet'])
            discrim = discriminant(h5file, fpath = fpath)
            weight = fat_jet['mcEventWeight']
            sel = selection(fat_jet)
            hist += np.histogram(
                discrim[sel], edges, weights=weight[sel])[0]
    return hist

def get_hist_reweighted(ds, edges, weights_hist, discriminant, selection, process, target='higgs'):
    with File(weights_hist, 'r') as h5file:
        num = h5file[target]['hist']
        denom = h5file[process]['hist']
        ratio_edges = np.asarray(h5file['higgs']['edges'])
        ratio = np.zeros_like(num)
        valid = np.asarray(denom) > 0.0
        ratio[valid] = num[valid] / denom[valid]

    hist = 0
    for fpath in glob(f'{ds}/*.h5'):
        with File(fpath,'r') as h5file:
            fat_jet = np.asarray(h5file['fat_jet'])
            pt = fat_jet['pt']
            indices = np.digitize(pt, ratio_edges) - 1
            weight = ratio[indices]
            disc = discriminant(h5file, fpath = fpath)
            sel = selection(fat_jet)
            hist += np.histogram(disc[sel], edges, weights=weight[sel])[0]
    return hist


DISCRIMINANT_GETTERS = {
    # 'tau32_top_vs_qcd': get_tau32,
    # 'jss_top_vs_qcd': get_jsstop,
    # 'xbb_anti_qcd': make_xbb_getter(ftop=0),
    # 'xbb_mixed': make_xbb_getter(ftop=0.5),
    # 'xbb_anti_top': make_xbb_getter(ftop=1.0),
    'prediction_anti_qcd': make_prediction_getter(ftop=0),
    'prediction_mixed': make_prediction_getter(ftop=0.5),
    'prediction_anti_top': make_prediction_getter(ftop=1.0),
    'dl1_VR': make_dl1_getter(SJ),
    'dl1_FR': make_dl1_getter(SJ_FR),
    'new_vr_dl1': make_vr_dl1r_getter(SJ),
    # 'xbb_top_vs_qcd': get_top_vs_qcd,
    # 'dl1ghost': make_dl1_getter(SJ_GHOST),
    # 'mv2': get_mv2,
    # 'dnn': get_dnn,
}
DISCRIMINANT_EDGES = {
    'dl1_VR': np.linspace(-10, 10, 1e3),
    'dl1_FR': np.linspace(-10, 10, 1e3),
    'new_vr_dl1': np.linspace(-10, 10, 1e3),
    'dl1': np.linspace(-10, 10, 1e3),
    'dl1ghost': np.linspace(-10, 10, 1e3),
    'mv2': np.linspace(-1, 1, 1e3),
    'dnn': np.linspace(0, 1, 1e3),
    'xbb_anti_qcd': np.linspace(-10, 10, 1e3),
    'xbb_anti_top': np.linspace(-10, 10, 1e3),
    'xbb_mixed': np.linspace(-10, 10, 1e3),
    'prediction_anti_qcd': np.linspace(-10, 10, 1e3),
    'prediction_anti_top': np.linspace(-10, 10, 1e3),
    'prediction_mixed': np.linspace(-10, 10, 1e3),
    'xbb_top_vs_qcd': np.linspace(-10, 10, 1e3),
    'tau32_top_vs_qcd': np.linspace(0, 1, 1e3),
    'jss_top_vs_qcd': np.linspace(0, 1, 1e3),
    'flat': np.linspace(0,1,100)
}

def write_discriminants(discrims, output_file):
    args = dict(dtype=float)
    for discrim in discrims:
        grp = output_file.create_group(discrim)
        for proc in ['higgs', 'dijet', 'top']:
            grp.create_dataset(proc, data=discrims[discrim][proc], **args)
        grp.create_dataset('edges', data=DISCRIMINANT_EDGES[discrim], **args)

def run():
    args = get_args()

    discrims = {}
    for discrim_name, getter in DISCRIMINANT_GETTERS.items():
        if args.verbose:
            print(f'running {discrim_name}')
        if 'top_vs_qcd' in discrim_name:
            window = mass_window_top
        else:
            window = mass_window_higgs
        dijet_selector = window_pt_range(args.pt_range, window)
        top_selector = window_pt_range_truth_match(
            args.pt_range, mass_window=window,
            truth_label='GhostTQuarksFinalCount')
        higgs_selector = window_pt_range_truth_match(
            args.pt_range, mass_window=window,
            truth_label='GhostHBosonsCount')

        edges = np.concatenate([np.array([-np.inf]),DISCRIMINANT_EDGES[discrim_name],np.array([np.inf])])
        discrims[discrim_name] = {
            'dijet': get_dijet(edges, args, getter, dijet_selector),
            'higgs': get_process(edges, 'higgs', args, getter, higgs_selector),
            'top': get_process(edges, 'top', args, getter, top_selector)
        }

    if args.save_file:
        with File(args.save_file, 'w') as output_file:
            write_discriminants(discrims, output_file)

    draw_roc_curves(discrims, args.output_dir)

def get_dijet(edges, args, discriminant=get_mv2,
              selection=mass_window_higgs):

    xsecs = get_xsecs(args.denominator, args.cross_sections)
    input_hists = args.input_hist_dir

    hist = 0
    for ds in args.datasets:
        dsid = get_dsid(ds)
        # if not is_dijet(dsid, restricted=True):
        if not is_dijet(dsid, restricted=False):
            continue
        if xsecs.datasets[dsid]['denominator'] == 0:
            continue
        if args.verbose:
            print(f'running on {ds}')
        weight = xsecs.get_weight(dsid)
        this_dsid = get_hist_reweighted(
            ds, edges, f'{input_hists}/jetpt.h5',
            discriminant, selection, process='dijet',
            target=args.target) * weight
        hist += this_dsid

    return hist

def get_process(edges, process, args, discriminant, selection):
    input_hists = args.input_hist_dir

    hist = 0
    for ds in args.datasets:
        dsid = get_dsid(ds)
        if not SELECTORS[process](dsid):
            continue
        if args.verbose:
            print(f'running on {ds} as {process}')

        this_dsid = get_hist_reweighted(
            ds, edges, f'{input_hists}/jetpt.h5',
            discriminant, selection, process, target=args.target)
        hist += this_dsid

    return hist

if __name__ == '__main__':
    run()
