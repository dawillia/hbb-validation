DISCRIMINANT_NAME_MAP = {
    'xbb_anti_qcd': r'XbbScore $p_{H} / p_{j}$',
    'xbb_anti_top': r'XbbScore $p_{H} / p_{t}$',
    'xbb_mixed': r'XbbScore $p_{H} / (p_{t} + p_{j})$',
    'xbb_top_vs_qcd': r'XbbScore Top Tagger',
    'prediction_anti_qcd': r'Prediction $p_{H} / p_{j}$',
    'prediction_anti_top': r'Prediction $p_{H} / p_{t}$',
    'prediction_mixed': r'Prediction $p_{H} / (p_{t} + p_{j})$',
    'dl1': r'DL1 double $b$-tag',
    'dl1_VR': r'DL1 VR double $b$-tag',
    'dl1_FR': r'DL1 FR double $b$-tag',
    'new_vr_dl1': r'New vr DL1 double $b$-tag',
    'mv2': r'MV2 double $b$-tag',
    'jss_top_vs_qcd': r'DNN Top Tagger',
    'tau32_top_vs_qcd': r'$\tau_{32}$',
}
DISCRIMINANT_COLOR_MAP = {
    'xbb_anti_qcd': 'blue',
    'xbb_anti_top': 'orange',
    'xbb_mixed': 'darkred',
    'prediction_anti_qcd': 'blue',
    'prediction_anti_top': 'orange',
    'prediction_mixed': 'darkred',
    'dl1': 'blue',
    'dl1_VR': 'blue',
    'dl1_FR': 'darkred',
    'new_vr_dl1': 'green',
    'mv2': 'darkgreen',
    'jss_top_vs_qcd': 'red',
    'tau32_top_vs_qcd': 'gray',
    'xbb_top_vs_qcd': 'blue',
}
PROCESS_NAME_MAP = {
    'dijet': 'Multijet',
    'top': 'Top'
}
