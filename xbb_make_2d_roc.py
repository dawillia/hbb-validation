#!/usr/bin/env python3

"""
Make roc 2d roc curves in pt vs eff space
"""

from argparse import ArgumentParser
from glob import glob
import os
from pathlib import Path

import numpy as np
from h5py import File

from xbb.common import get_denom_dict, get_dsid
from xbb.cross_section import get_xsecs
from xbb_make_roc_curves import DISCRIMINANT_GETTERS, DISCRIMINANT_EDGES
from xbb.selectors import (mass_window_higgs, window_pt_range,
                           window_pt_range_truth_match)
from xbb.common import SELECTORS
from xbb_draw_2d_roc import draw_2d_rocs

from xbb.mpl import Canvas
from matplotlib.colors import LogNorm

def get_args():
    parser = ArgumentParser(description=__doc__)
    d = 'default: %(default)s'
    parser.add_argument('datasets', nargs='+')
    parser.add_argument('-d', '--denominator', required=True)
    parser.add_argument('-x', '--cross-sections', required=True)
    parser.add_argument('-i', '--input-hist-dir', default='pt-hists')
    parser.add_argument('-s', '--save-file')
    parser.add_argument('-o', '--out-dir', type=Path, default='plots')
    parser.add_argument('-v', '--verbose', action='store_true')
    return parser.parse_args()


def get_2d_hist(ds, weights_hist, discrim_name, selection, process):
    with File(weights_hist, 'r') as h5file:
        num = h5file['higgs']['hist']
        denom = h5file[process]['hist']
        ratio_edges = np.asarray(h5file['higgs']['edges'])
        ratio = np.zeros_like(num)
        valid = np.asarray(denom) > 0.0
        ratio[valid] = num[valid] / denom[valid]

    discriminant = DISCRIMINANT_GETTERS[discrim_name]
    edges = get_edges(discrim_name)
    edges_overflow = [np.concatenate([[-np.inf], e, [np.inf]]) for e in edges]
    hist = 0
    for fpath in glob(f'{ds}/*.h5'):
        with File(fpath,'r') as h5file:
            fat_jet = np.asarray(h5file['fat_jet'])
            pt = fat_jet['pt']
            indices = np.digitize(pt, ratio_edges) - 1
            weight = ratio[indices]
            weight *= fat_jet['mcEventWeight']
            disc = discriminant(h5file, fpath = fpath)
            sel = selection(fat_jet)
            input_arr = np.stack((disc[sel], pt[sel])).T
            hist += np.histogramdd(
                input_arr, edges_overflow, weights=weight[sel])[0]
    return hist

def get_edges(discrim_name):
    edges = [
        np.linspace(-10, 10, 2000), # discrim
        np.linspace(250e3, 2500e3, 500)]  # pt
        # np.linspace(250e3, 3000e3, 500)]  # pt
    return edges

def draw_2d_hist(hist, out_dir, file_name, discrim_name):

    bins = get_edges(discrim_name)
    extent = (0.1, 1, bins[1].min(), bins[1].max())

    args = dict(
        aspect='auto',
        origin='lower',
        extent=extent,
        interpolation='nearest',
        norm=LogNorm())
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)

    with Canvas(f'{out_dir}/{file_name}') as can:
        im = can.ax.imshow(hist[1:-1,1:-1].T, **args)
        can.fig.colorbar(im)


def get_process(process, args, discrim_name, selection, cross_sections):
    input_hists = args.input_hist_dir

    hist = 0
    for ds in args.datasets:
        dsid = get_dsid(ds)
        is_dijet = process == 'dijet'
        if not SELECTORS[process](dsid, restricted=is_dijet):
            continue
        if args.verbose:
            print(f'running on {ds} as {process}')

        this_dsid = get_2d_hist(
            ds, f'{input_hists}/jetpt.h5',
            discrim_name, selection, process=process)
        if is_dijet:
            this_dsid *= cross_sections.get_weight(dsid)
        hist += this_dsid

    return hist


def integrate(hist):
    hist = np.cumsum(hist[::-1,:], axis=0)[::-1,:]
    hist = np.cumsum(hist[:,::-1], axis=1)[:,::-1]
    return hist

# default settings
PT_RANGE = (250e3, np.inf)

def run():
    args = get_args()
    window = mass_window_higgs
    dijet_selector = window_pt_range(PT_RANGE, window)
    top_selector = window_pt_range_truth_match(
        PT_RANGE, mass_window=window,
        truth_label='GhostTQuarksFinalCount')
    higgs_selector = window_pt_range_truth_match(
        PT_RANGE, mass_window=window,
        truth_label='GhostHBosonsCount')

    xsecs = get_xsecs(args.denominator, args.cross_sections)

    # discrim_names = ['xbb_mixed', 'dl1']
    discrim_names = ['prediction_mixed', 'dl1_VR']
    discrims = {}
    for discrim_name in discrim_names:
        common = dict(cross_sections=xsecs,
                      discrim_name=discrim_name,
                      args=args)
        discrims[discrim_name] = {
            'dijet': get_process('dijet', selection=dijet_selector, **common),
            'higgs': get_process('higgs', selection=higgs_selector, **common),
            'top': get_process('top', selection=top_selector, **common)
        }
    if args.save_file:
        with File(args.save_file, 'w') as save_file:
            for discrim_name, discrim in discrims.items():
                limits = [(x.min(), x.max()) for x in get_edges(discrim_name)]
                disc_group = save_file.create_group(discrim_name)
                for pname, proc in discrim.items():
                    ds = disc_group.create_dataset(
                        pname, data=proc, compression='gzip')
                    ds.attrs['limits'] = limits

    draw_2d_rocs(discrims, args.out_dir, limits)

if __name__ == '__main__':
    run()
